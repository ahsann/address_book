<?php
namespace App\BITM\SEIP106611\AddressBook;
use \App\BITM\SEIP106611\Utility\Utility;

class AddressBook{
    
    public $id = "";
    public $name = "";
    public $phone = "";
    public $photo = "";
    public $deleted_at = null;
    
    public function __construct($data = false){
        if(is_array($data) && array_key_exists("id", $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
   
		//$this->photo = $files['photo'];
        $this->fname = trim($data['fname']);
        $this->lname = trim($data['lname']);
        $this->hno = trim($data['hno']);
        $this->ofno = trim($data['ofno']);
        $this->otno = trim($data['otno']);
        $this->mno = trim($data['mno']);
        $this->email = trim($data['email']);
        $this->gender = trim($data['gender']);
        $this->city = trim($data['city']);
        $this->address = trim($data['address']);
        $this->service = trim($data['service']);
        
    }
    
    public function index( $data=array() ){
		$conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("phonebook") or die("Cannot select database.");
		
        
       $whereClause = "AND 1=1";
	  
        if(array_key_exists('search',$data)){
            $whereClause .= " AND first_name LIKE '%".$data['search']."%'";
            $whereClause .= " OR last_name LIKE '%".$data['search']."%'";
            $whereClause .= " OR mobile_phone LIKE '%".$data['search']."%'";
			
        } 
		else{
            $filter = $data;
            if(is_array($filter) && count($filter) > 0){

              if(array_key_exists('filterTitle', $filter) && !empty($filter['filterTitle']) ){
                  $whereClause .= " AND title LIKE '%".$filter['filterTitle']."%'";
              }

              if(array_key_exists('filterAuthor', $filter)  && !empty($filter['filterAuthor']) ){
                  $whereClause .= " AND author LIKE '%".$filter['filterAuthor']."%'";
              }

            }
        }
		 

	   $phones = array();
	   
        $query = "SELECT * FROM `phone` WHERE deleted_at IS NULL";
		
        $result = mysql_query($query);
		
        
        while($row = mysql_fetch_object($result)){
            $phones[] = $row;
        }
        return $phones;
		
    }
	public function getAllTitle(){
		$conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("phonebook") or die("Cannot select database.");
		
        $phones = array();

        $query = "SELECT first_name FROM `phone` WHERE deleted_at IS NULL ";
        
        //utility::dd($query);
        $result = mysql_query($query);
        
        while($row = mysql_fetch_assoc($result)){
            $phones[] = $row['first_name'];
        }
        return $phones;
    }
	
    public function trashed(){
        
        $phones = array();
        
		$conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("phonebook") or die("Cannot select database.");
        
        $query = "SELECT * FROM `phone` WHERE deleted_at IS NOT NULL";
        $result = mysql_query($query);
        
        while($row = mysql_fetch_object($result)){
            $phones[] = $row;
        }
        return $phones;
    }
	
    
    public function store(){
       
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("phonebook") or die("Cannot select database.");
		
		
		/* 	if(!file_exists("upload")){
                mkdir("upload");
			}
            $ext = strtolower(pathinfo($this->photo["name"],PATHINFO_EXTENSION));
            $file_name = basename(strtolower(substr($this->photo["name"], 0,15)));
            $path = trim($file_name.".".$ext);
            if(!file_exists("upload/".$path)){
              move_uploaded_file($this->photo["tmp_name"], "upload/".$path);
            }
		
         */
        $query = "INSERT INTO `phonebook`.`phone` (`first_name`,`last_name`,`home_phone`,`office_phone`,`others_phone`,`mobile_phone`,`email`,`gender`,`city`,`address`,`service`) VALUES ('".$this->fname."','".$this->lname."','".$this->hno."','".$this->ofno."','".$this->otno."','".$this->mno."','".$this->email."','".$this->gender."','".$this->city."','".$this->address."','".$this->service."')";
		
		//var_dump($query);die();
		
        $result = mysql_query($query);
		
		if($result){
			echo "success";
		}
		else{
			echo "sorry";
		}
        
        if($result){
            Utility::message("<div class=\"message_success\">Data Has Been Inserted Successfully.</div>");
        }else{
            Utility::message("<div class=\"message_error\">Opss, Error Detected. Please try again...</div>");
        }
        
        Utility::redirect('index.php');
    }

    public function show($id = false){
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("phonebook") or die("Cannot select database.");
        $query = "SELECT * FROM `phone` WHERE id=".$id;
        $result = mysql_query($query);
        $row = mysql_fetch_object($result);
        return $row;

    }

    public function update(){
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("phonebook") or die("Cannot select database.");
        
		
		 $query = "UPDATE `phonebook`.`phone` SET `first_name`='".$this->fname."',`last_name`='".$this->lname."',`home_phone`='".$this->hno."',`office_phone`='".$this->ofno."',`others_phone`='".$this->otno."',`mobile_phone`='".$this->mno."',`email`='".$this->email."',`gender`='".$this->gender."',`city`='".$this->city."',`address`='".$this->address."',`service`='".$this->service."' WHERE `phone`.`id` = ".$this->id;

        $result = mysql_query($query);
               
        if($result){
            Utility::message("<div class=\"message_success\">Data Has Been Updated Successfully.</div>");
        }else{
            Utility::message("<div class=\"message_error\">Opss, Error Detected. Please try again....</div>");
        }
        
        Utility::redirect('index.php');
    }

    public function delete($id = null){
       
        if(is_null($id)){
            Utility::message("<div class=\"message_error\">No id avaiable. Sorry !</div>");
            Utility::redirect('index.php');
        }
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("phonebook") or die("Cannot select database.");

        $query = "DELETE FROM `phonebook`.`phone` WHERE `phone`.`id` = ".$id;
        $result = mysql_query($query);
		
               
        if($result){
            Utility::message("<div class=\"message_success\">Data Has Been Deleted Successfully.</div>");
        }else{
            Utility::message("<div class=\"message_error\">Opss, Error Detected. Please try again...</div>");
        }
        
        Utility::redirect('index.php');
    }
	
	
	public function deletemultiple($ids = array()){
       
            if(is_array($ids) && count($ids) > 0){

                 //Utility::dd($ids);
                 $_ids = implode(',',$ids);



                 $query = "DELETE FROM `phonebook`.`phone` WHERE `phone`.`id` IN($_ids) ";

                 //Utility::dd( $query);
                 $result = mysql_query($query);

                 if($result){
                     Utility::message("Data has been deleted successfully.");
                 }else{
                     Utility::message(" Opss, Error Detected. can not Delete. Please try again...");
                 }

                 Utility::redirect('index.php');
             }else{
                 Utility::message('No id avaiable. Sorry !');
                 return Utility::redirect('index.php');
             }
    }
	
	   

	public function trash($id = null){
			$conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("phonebook") or die("Cannot select database.");
       
        if(is_null($id)){
            Utility::message("<div class=\"message_error\">No id avaiable. Sorry !</div>");
            Utility::redirect('index.php');
        }
		
		$this->id = $id;
        $this->deleted_at=time();
		
	
        $query = "UPDATE `phonebook`.`phone` SET `deleted_at` = '".$this->deleted_at."' WHERE `phone`.`id` = ".$this->id;
		
		$result=mysql_query($query);

		if($result){
            Utility::message("<div class=\"message_success\">Data Has Been Trashed Successfully.</div>");
        }else{
            Utility::message("<div class=\"message_error\">Opss, Error Detected. Please try again...</div>");
        }
        
        Utility::redirect('index.php');
    }
	
	   
	public function recover($id = null){
			$conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("phonebook") or die("Cannot select database.");
       
        if(is_null($id)){
            Utility::message("<div class=\"message_error\">No id avaiable. Sorry !</div>");
            Utility::redirect('index.php');
        }
		
		$this->id = $id;
	
        $query = "UPDATE `phonebook`.`phone` SET `deleted_at` = NULL WHERE `phone`.`id` = ".$this->id;
		
		$result=mysql_query($query);
            
        if($result){
            Utility::message("<div class=\"message_success\">Data Has recover Successfully.</div>");
        }else{
            Utility::message("<div class=\"message_error\">Opss, Error Detected. can not Recover. Please try again...</div>");
        }
        
        Utility::redirect('index.php');
    }
	
	public function recovermultiple($ids = array()){
       
        if(is_array($ids) && count($ids) > 0){
            
            //Utility::dd($ids);
            $_ids = implode(',',$ids);



            $query = "UPDATE `phonebook`.`phone` SET `deleted_at` = NULL WHERE `phone`.`id` IN($_ids) ";

            //Utility::dd( $query);
            $result = mysql_query($query);

            if($result){
                Utility::message("Data has been recovered successfully.");
            }else{
                Utility::message(" Opss, Error Detected. can not recover Please try again....");
            }

            Utility::redirect('index.php');
        }else{
            Utility::message('No id avaiable. Sorry !');
            return Utility::redirect('index.php');
        }
        

    }

	

}
