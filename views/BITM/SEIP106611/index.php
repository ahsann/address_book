<?php
session_start();
include_once '../../../Resource/db/dbconnect.php';

if(!isset($_SESSION['user']))
{
	header("Location: index.php");
}
$res=mysql_query("SELECT * FROM users WHERE user_id=".$_SESSION['user']);
$userRow=mysql_fetch_array($res);
?>

<?php
include_once('../../../vendor/autoload.php');
use \App\BITM\SEIP106611\AddressBook\AddressBook;
use \App\BITM\SEIP106611\Utility\Utility;

 $phone = new AddressBook();
 $phones = $phone->getAllTitle();
 
    
    $filter=array();
    $filterAuthor = isset($_POST['filterAuthor'])?$_POST['filterAuthor']:"";
    $filterTitle  = isset($_POST['filterTitle'])?$_POST['filterTitle']:"";
    
    $search = "";
 
	if( strtoupper($_SERVER['REQUEST_METHOD']) == 'POST'){
          $filter = $_POST;
          $phones = $phone->index($filter);
    }
    
    if( strtoupper($_SERVER['REQUEST_METHOD']) == 'GET'){
          $search = isset($_GET['search'])?$_GET:array('search'=>'');
          $phones = $phone->index($search);
    }
    
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Phone Book</title>

        <link href="../../../Resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        
        <link href="../../../Resource/css/style.css" rel="stylesheet">
		
        
    </head>
	
	<body>
		
		<div class="all">
	
			<div class=" wrapper">
			
			<div id="header">
				<div id="left">
				<label>The BackBenchers</label>
				</div>
				<div id="right">
					<div id="content">
						hi' <?php echo $userRow['user_name']; ?>&nbsp;<a href="logout.php?logout">Sign Out</a>
					</div>
				</div>
			</div>
			
				<div class="header">
					<h4>Phone Book</h4>
				</div>
				<div id="message">
				<?php echo Utility::message(); ?>            
				</div>
				
				<div class="main_content">
					<div class="form-horizontal">
						<div class="form-group">
										<div class="col-md-4">
										</div>
										<div class="col-md-8">
											<form action="index.php" method="get" >
													<div>
														
														<input class="col-md-8" type="text" name="search" value="<?php echo $search['search'];?>" /> 
														
														<button class="col-md-4" type="submit">Search</button>
													</div>
											</form>
										</div>
										<br />
										<br />
										<div class="row  col-md-12">
										
											<div class="col-md-1" style="float:left">
												<form class="ajax" action="#" method="post">
													<select class="items" name="items">
														<option value="15">10</option>
														<option value="20">20</option>
														<option value="30">30</option>
														<option value="40">40</option>
													</select>
												</form>
											</div>
											
									
											
											
																								
												<div class="row col-md-11" style="float:right">
													
													<div class="col-md-12">
														<p class="col-md-4">Download</p>		
														<a class=" col-md-2 btn btn-success btn-xs" href="pdf.php">PDF </a>
																			
														<a class="col-md-3 btn btn-success btn-xs" href="xl.php">EXCEL </a>
														<a class="col-md-3 btn btn-warning btn-xs" href="trashed.php" class="list-btn">Trashed</a>
																			
													</div>
												</div>
											
										</div>
						</div>
					</div>
				
					<div class="row main_info">
						<div class="col-md-12">
							<div class="box-table">
								<table class="table table-border table-hover">
									<thead>
										<tr>
											<th>Serial</th>
											<th>Photo</th>
											<th>Name</th>
											<th>Number</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
									<?php
											$count =1;
											if(count($phones) > 0){
											foreach($phones as $phone){
									?>
										<tr>
											<td><?php echo $count;?></td>
											<td><?php echo $phone->photo;?></td>
											<td><a href=""><?php echo $phone->first_name." ".$phone->last_name;?></a></td>
											<td><?php echo $phone->mobile_phone;?></td>
											<td>
												<div class="bs-example">

													<div class="btn-group">
														<button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle btn-xs">Option <span class="caret"></span></button>
														<ul class="dropdown-menu">
															<li><a class="btn btn-primary btn-xs" href="show.php?id=<?php echo $phone->id;?>" class="list-btn">View</a></li>
															<li><a class="btn btn-info btn-xs" href="edit.php?id=<?php echo $phone->id;?>" class="list-btn">Edit</a> </li>
															<li><a class="btn btn-danger btn-xs" href="delete.php?id=<?php echo $phone->id;?>" class="list-btn delete">Delete</a></li>
															<li><a class="btn btn-warning btn-xs" href="trash.php?id=<?php echo $phone->id;?>" class="list-btn">Trash</a></li>
															<li><a href="#">Send</a></li>
														</ul>
													</div>
												</div>
											</td>
										</tr>
										<?php
											$count++;
												}
											}else{
										?>
										<tr><td colspan="6"> No record found</td></tr>
										<?php
											}
										?>
									</tbody>
								</table>
							</div>
						
						</div> 
						
					</div>
				</div>
				<div class="main_content">
				<div class="" style="float:left">
					<h5>Prev 1>2>3> Next></h5>
				</div>
				<div class="" style="float:right">
					<a class="btn btn-success btn-xs" href="create.php">Create New</a>
				</div>
				</div>
			</div>
		</div>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="../../../Resource/bootstrap/js/bootstrap.min.js"></script>
		          <script>
  $(function() {
    var availableTags = [
         '<?php echo implode("','",$phones);?>'   
    ];
    $( "#filterTitle" ).autocomplete({
      source: availableTags
    });
  });
  </script>
        <script>
           $('.delete').bind('click',function(e){
               var deleteItem = confirm("Are you sure you want to delete?");
               if(!deleteItem){
                  //return false; 
                  e.preventDefault();
               }
           }); 
    
    
    $('#message').hide(5000);
    
        </script>
	</body>
</html>	