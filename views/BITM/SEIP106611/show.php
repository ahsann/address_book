<?php
include_once('../../../vendor/autoload.php');
use \App\BITM\SEIP106611\AddressBook\AddressBook;
use \App\BITM\SEIP106611\Utility\Utility;

$obj = new AddressBook();
$phone = $obj->show($_GET["id"]);

 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Book List</title>
	<link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../resource/css/style.css">
  </head>
  <body>
      <div align="center" class="wrapper1">
          <h1>Your Contact Info: </h1>
          <table class="table">
            <tr class="success">
              <td>ID : <?php echo $phone->id; ?></td>
            </tr>
            <tr class="info">
              <td>First Name : <?php echo $phone->first_name; ?></td>
            </tr>
			<tr class="info">
              <td>Last Name : <?php echo $phone->last_name; ?></td>
            </tr>
            
			<tr class="success">
              <td>Home Phone Number: <?php echo $phone->home_phone; ?></td>
            </tr>
			<tr class="success">
              <td>Office Phone Number : <?php echo $phone->office_phone; ?></td>
            </tr>
			<tr class="success">
              <td>Others Phone Number : <?php echo $phone->others_phone; ?></td>
            </tr>
			<tr class="success">
              <td>Mobile Number : <?php echo $phone->mobile_phone; ?></td>
            </tr>
			<tr class="success">
              <td>Photo : <?php echo $phone->photo; ?></td>
            </tr>
			<tr class="success">
              <td>Email : <?php echo $phone->email; ?></td>
            </tr>
			<tr class="success">
              <td>Gender : <?php echo $phone->gender; ?></td>
            </tr>
			<tr class="success">
              <td>City : <?php echo $phone->city; ?></td>
            </tr>
			<tr class="success">
              <td>Address : <?php echo $phone->address; ?></td>
            </tr>
			<tr class="success">
              <td>Service : <?php echo $phone->service; ?></td>
            </tr>
          </table>

          <p class="text-center btn btn-success"><a href="index.php">Go to Phone Book</a></p>
      </div>
      
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../../Resource/js/bootstrap.min.js"></script>
  </body>
</html>