-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2016 at 06:22 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phonebook`
--

-- --------------------------------------------------------

--
-- Table structure for table `phone`
--

CREATE TABLE `phone` (
  `id` int(11) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL,
  `home_phone` int(11) NOT NULL,
  `office_phone` int(11) NOT NULL,
  `others_phone` int(11) NOT NULL,
  `mobile_phone` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `service` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `phone`
--

INSERT INTO `phone` (`id`, `photo`, `first_name`, `last_name`, `deleted_at`, `home_phone`, `office_phone`, `others_phone`, `mobile_phone`, `email`, `gender`, `city`, `address`, `service`) VALUES
(6, '', 'Pavel', 'Parvej', NULL, 1749896582, 1749896582, 1749896582, 1749896582, 'p.parvej007@gmail.com', 'Male', 'Dhaka', 'Mirpur', 'Student'),
(7, '', 'Pavel', 'Parvej', NULL, 0, 0, 0, 1749896582, '', 'Male', '', '', 'Student'),
(8, '', 'Ahsan', 'Ullah', NULL, 1749896582, 1749896582, 1749896582, 1749896582, 'sujon_mian@yahoo.com', 'Male', 'Dhaka', 'Nikunjo', 'Engineer'),
(9, '', 'Pavel', 'Parvej', NULL, 1749896582, 1749896582, 1749896582, 1749896582, 'p.parvej007@gmail.com', 'Male', 'Dhaka', 'Mirpur', 'Engineer'),
(10, '', 'Sujon', 'Mian', NULL, 1740051616, 1740051616, 1740051616, 1740051616, 'sujon_mian@yahoo.com', 'Male', 'Dhaka', 'Framget', 'Engineer'),
(11, '', 'Faisal', 'Ahmed', NULL, 1913325819, 1913325819, 1913325819, 1913325819, 'kamranromim@gmail.com', 'Male', 'Dhaka', 'Mirpur', 'Scientist');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE `tbl_login` (
  `login_id` int(2) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`login_id`, `email_address`, `password`) VALUES
(1, 'pavel@gmail.com', '123');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(5) NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(35) NOT NULL,
  `user_pass` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `user_name`, `user_email`, `user_pass`) VALUES
(6, 'Ahsan', 'pavel_cse', 'pavel@cse.com', '123'),
(5, 'Pavel Parvej', 'pavel_cse', 'pavel.cse26@gmail.com', '123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `phone`
--
ALTER TABLE `phone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`login_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `phone`
--
ALTER TABLE `phone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tbl_login`
--
ALTER TABLE `tbl_login`
  MODIFY `login_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
